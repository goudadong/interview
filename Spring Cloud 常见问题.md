### Spring Cloud 核心原理

- `spring cloud` 包括核心组件`Eureka`、 `Ribbon`、`Feign` 、`Hystrix`、`Zuul` 、`Spring cloud config` 、`Spring Cloud Sleuth` 

-  `Eureka` 注册中心 核心原理：

  当服务注册到eureka服务注册表中，它会立即写入到写缓存中，这时readonly缓存中还没更新，需要等待线程1默认30秒定时刷新新的服务列表到readolny缓存中，然后服务B每隔30秒拉取一次最新的服务列表

  默认情况下，线程2会60秒检查一下服务列表是否有变更，如果表更了就会刷新到WriteOnly 缓存中，eureka 会每隔30秒检查一下心跳，如果心跳检查默认配置的90秒中都没有收到新的心跳，就会将服务注册表中对应的没有心跳的服务给删除掉，然后60秒之后就会将删除掉没心跳的服务剩下的服务刷新到写缓存中（先清空缓存在写入）。

  ![Eureka核心原理](images/Eureka核心原理.png)

  - Eureka 客户端配置

  ```yaml
  # 客户端的配置
  eureka:
    instance:
      hostname: localhost
      #每隔3秒钟发送一次心跳
      lease-renewal-interval-in-seconds: 3
    client:
      serviceUrl:
        defaultZone: http://localhost:8761/eureka
      #每隔3秒钟拉取一次最新的服务注册表
      registry-fetch-interval-seconds: 3
  ```

  - Eureka 服务端的配置

  ```yaml
  #服务端的配置
  eureka:
    instance:
      hostname: localhost
      # 表示自eureka服务器接收到上一次心跳以来可以等待的时间（以秒为单位），
      # 之后它才能从其视图中删除该实例，并通过禁止对该实例的流量进行操作。
      # 将此值设置得太长可能意味着即使实例未处于活动状态，也可以将流量路由到实例。
      # 将此值设置得太小可能意味着，由于临时网络故障*该实例可能会失去流量。此值应至少设置为 leaseRenewalIntervalInSeconds中指定的值
      # 默认是90秒
      # eureka线程检查上一次接收到心跳到现在等待的时间，如果超过这个时间，那么就会将这个服务从服务注册表删除掉
      lease-expiration-duration-in-seconds: 2
    client:
      registerWithEureka: false
      fetchRegistry: false
      serviceUrl:
        defaultZone: http://localhost:8761/eureka/
    server:
      # 关闭自我保护机制
      enableSelfPreservation: false
      #每隔100毫秒更新一次缓存
      response-cache-update-interval-ms: 100
      # 每隔1秒检查一下服务注册表中服务是否在正常的发送心跳，如果超过了lease-expiration-duration-in-seconds 的时间内都没有收到心跳，那么就认为这个服务死了，就会将它从服务列表删除掉
      eviction-interval-timer-in-ms: 1000
  ```

  ​

- Ribbon 负载均衡原理：

  使用轮询、随机等策略进行负载均衡，首次请求很慢，因为默认是懒加载的，可以使用饥饿加载的方式提高首次访问的速度

  - Ribbon 配置

 ```yaml
  #开启ribbon 组件饥饿加载
  ribbon:
    eager-load:
      enabled: true
    #请求连接的超时时间
    ConnectTimeout: 1000
    #请求处理的超时时间
    ReadTimeout: 1000
    #对所有操作都进行重试
    OkToRetryOnAllOperations: true
    #对当前实例的重试次数
    MaxAutoRetries: 1
    #切换实例的重试次数
    MaxAutoRetriesNextServer: 1
  ```

优化eureka注册中心服务端
将线程检查上一次心跳到现在的等待时间设置为5秒（默认90秒）
关闭自我保护机制
将更新缓存的间隔时间设置为3秒（默认30秒）
将检查心跳的间隔时间设置为3秒（默认30秒）
优化eureka注册中心客户端
将发送心跳的时间设置为3秒（默认30秒）
将拉取注册表的时间设置为3秒（默认30秒）

优化客户端Ribbon
开启饥饿加载
设置请求连接超时时间为1秒
设置请求处理超时时间为1秒
开启对所有的操作进行重试
设置最大重试次数为1次
设置切换实例的重试次数为1次
关闭feign熔断机制

  ​