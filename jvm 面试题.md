#### jvm 面试

#### 1、说说你们生产环境的jvm参数是如何配置的？为什么这么设置？
-Xms2048m -Xmx2048m -Xmn1500m -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:SurvivorRatio=6 -Xss1m -XX:MetaSpaceSize=128m
-XX:PretenureSizeThreshold=10m  -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/temp/oom.dump


####  2、你在生产环境的jvm优化经验可以聊聊吗？
在综合测评系统中，由于导出数据的时候，数据太大，加上工程师写的代码创建了太多的对象，一下子创建了大概几百m的对象，然后启动的时候没有对jvm参数进行调优过，使用默认的启动，
导致新生代内存不足，直接进入老年代，老年代很快就满了，然后不断的发生fullgc


####  3、说说你在生产环境解决过的jvm oom 问题?

