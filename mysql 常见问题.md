



### 说说mysql底层Buffer Pool 原理？

<img src="images/mysql%20innodb%20buffer%20pool%20运行原理.png"  />

 - buffer pool中的日志文件和链表：
   - undo log 日志，存储磁盘中数据页的旧数据，用于回滚恢复旧数据，以及MVCC多版本控制链实现RC下解决幻读问题
   - redo log 日志，存储内存执行更新操作后的脏数据，用于数据库宕机后恢复内存中修改过的数据
   - binlog 归档日志（二进制日志），存储更新的sql语句，记录了所有的ddl和dml（除了查询）语句，用于主从复制，数据恢复及备份
   - free 链表，存储mysql启动后数据页对应的缓存页的描述数据，表示数据页还未加载到内存中
   - flush 链表，存储mysql执行更新后的脏数据的缓存页，后台线程会定时的刷入数据到磁盘，表示执行了更新操作
   - lru 链表，存储mysql从磁盘中加载到内存中的数据页对应的缓存页，为了解决哪些缓存页使用频繁或者不频繁，然后进行缓存页淘汰策略，因为数据不可能全部都加载内存中
   - 缓存页的hash 表，以表空间号+数据页号为key，缓存页地址为value，存储哪些数据页已经加载到了内存中，如果有了就直接使用

1、buffer Pool中在执行sql语句的时候，首先会判断hash表中是否有缓存页，如果存在，那么就直接使用，否则就会去磁盘中加载数据页，默认数据页为16kb
2、当加载进buffer pool中的时候，会将数据写入到undo log 日志中，并且删除free链表中数据页对应的缓存页
3、加载进内存之后就会写入到hash表中，并且将缓存页写入到lru链表中，lru链表分为冷热数据区，刚加载进来的数据会放入冷数据区
4、当执行更新，将缓存页中的数据更新了，内存中的数据就变成了脏数据，就会将对应的缓存页写入到flush链表中，并且写入到redo log日志中
5、当提交事务的时候，redo log 日志就会按照刷盘磁盘进行刷盘，并且在binlog日志中记录当前的更新sql语句，当提交完成之后会将binlog日志文件名称和文件位置写入到redo log文件尾，并添加一个commit标记

总结：
当执行一条更新SQL语句的时候，首先会在hash表中查看，如果有数据，就直接使用缓存中的数据，如果没有就加载磁盘上的数据页到缓存页中，
并且写入到undo log中，然后从free链表中删除缓存页，然后在加入到lru链表中，当执行完更新之后，就会加入到flush链表中以及按照刷盘策略写入到redo log中，
当事务提交之后还会写入到binlog二进制文件中，并且写入commit标识，表示事务真正的完成。
free链表不断的删除，lru链表不断的增加，flush链表也不断的增加，然后后台线程又在不断的将flush链表刷入磁盘，lru链表也是不断的刷入磁盘（当flush链表有对应的缓存页的时候，否则直接删除）
当flush链表刷入磁盘后，free链表的又不断的增加（因为有数据的缓存页刷入磁盘，空的缓存页有多出来了）

- buffer pool 组成结构

![](images/buffer pool 组成结构.png)

默认情况下chunk是128m，可以通过 innodb_buffer_pool_chunk_size = 128m 来控制， buffer pool 是由多个chunk组成的，buffer pool 总大小=（128 * buffer pool 数量）的倍数， buffer pool 默认如果小于1g，只会分配一个buffer pool ，
如果机器大，可以通过调整服务端参数进行调整，因为默认是加锁的，调整了buffer pool 的数量，可以增加并发能力 ， 多个chunk和多个buffer pool 共享一套free链表、flush链表、lru链表 

```sh
[mysqld]
innodb_buffer_pool_size=8G
innodb_buffer_pool_instances=4
```



- show ENGINE innodb status  查看innodb参数

```markdown

=====================================
2021-01-20 01:35:57 0x7fb5aeeb3700 INNODB MONITOR OUTPUT
=====================================
Per second averages calculated from the last 50 seconds
-----------------
BACKGROUND THREAD
-----------------
srv_master_thread loops: 9299 srv_active, 0 srv_shutdown, 114705 srv_idle
srv_master_thread log flush and writes: 124004
----------
SEMAPHORES
----------
OS WAIT ARRAY INFO: reservation count 11150
OS WAIT ARRAY INFO: signal count 20653
RW-shared spins 0, rounds 9680, OS waits 4892
RW-excl spins 0, rounds 160187, OS waits 5256
RW-sx spins 39, rounds 388, OS waits 11
Spin rounds per wait: 9680.00 RW-shared, 160187.00 RW-excl, 9.95 RW-sx
------------
TRANSACTIONS
------------
Trx id counter 2531560
Purge done for trx's n:o < 2531213 undo n:o < 0 state: running but idle
History list length 0
LIST OF TRANSACTIONS FOR EACH SESSION:
---TRANSACTION 421894073872936, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073872016, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073871096, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073870176, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073876616, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073873856, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073895016, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073906976, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073906056, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073905136, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073898696, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073874776, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073889496, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073888576, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073885816, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073884896, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073883976, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073880296, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421894073879376, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
--------
FILE I/O
--------
I/O thread 0 state: waiting for completed aio requests (insert buffer thread)
I/O thread 1 state: waiting for completed aio requests (log thread)
I/O thread 2 state: waiting for completed aio requests (read thread)
I/O thread 3 state: waiting for completed aio requests (read thread)
I/O thread 4 state: waiting for completed aio requests (read thread)
I/O thread 5 state: waiting for completed aio requests (read thread)
I/O thread 6 state: waiting for completed aio requests (write thread)
I/O thread 7 state: waiting for completed aio requests (write thread)
I/O thread 8 state: waiting for completed aio requests (write thread)
I/O thread 9 state: waiting for completed aio requests (write thread)
Pending normal aio reads: [0, 0, 0, 0] , aio writes: [0, 0, 0, 0] ,
 ibuf aio reads:, log i/o's:, sync i/o's:
Pending flushes (fsync) log: 0; buffer pool: 0
64917 OS file reads, 226089 OS file writes, 206958 OS fsyncs
0.00 reads/s, 0 avg bytes/read, 0.06 writes/s, 0.00 fsyncs/s
-------------------------------------
INSERT BUFFER AND ADAPTIVE HASH INDEX
-------------------------------------
Ibuf: size 1, free list len 1655, seg size 1657, 2309 merges
merged operations:
 insert 17156, delete mark 0, delete 0
discarded operations:
 insert 0, delete mark 0, delete 0
Hash table size 34679, node heap has 3 buffer(s)
Hash table size 34679, node heap has 6 buffer(s)
Hash table size 34679, node heap has 53 buffer(s)
Hash table size 34679, node heap has 101 buffer(s)
Hash table size 34679, node heap has 1132 buffer(s)
Hash table size 34679, node heap has 6 buffer(s)
Hash table size 34679, node heap has 31 buffer(s)
Hash table size 34679, node heap has 7 buffer(s)
0.00 hash searches/s, 3.08 non-hash searches/s
---
LOG
---
Log sequence number 14120721071
Log flushed up to   14120721071
Pages flushed up to 14120721071
Last checkpoint at  14120721062
0 pending log flushes, 0 pending chkp writes
202891 log i/o's done, 0.00 log i/o's/second
----------------------
BUFFER POOL AND MEMORY
----------------------
Total large memory allocated 137428992 
Dictionary memory allocated 1133106
Buffer pool size   8192
Free buffers       1025
Database pages     5828
Old database pages 2131
Modified db pages  0
Pending reads      0
Pending writes: LRU 0, flush list 0, single page 0
Pages made young 82423, not young 20985301
0.00 youngs/s, 0.00 non-youngs/s
Pages read 64753, created 2358, written 21079
0.00 reads/s, 0.00 creates/s, 0.00 writes/s
Buffer pool hit rate 1000 / 1000, young-making rate 0 / 1000 not 0 / 1000
Pages read ahead 0.00/s, evicted without access 0.00/s, Random read ahead 0.00/s
LRU len: 5828, unzip_LRU len: 0
I/O sum[3]:cur[0], unzip sum[0]:cur[0]
--------------
ROW OPERATIONS
--------------
0 queries inside InnoDB, 0 queries in queue
0 read views open inside InnoDB
Process ID=1, Main thread ID=140418654140160, state: sleeping
Number of rows inserted 251524, updated 78503, deleted 9, read 117295634
0.08 inserts/s, 0.00 updates/s, 0.00 deletes/s, 2.48 reads/s
----------------------------
END OF INNODB MONITOR OUTPUT
============================

```



- 调整参数：

  ```shell
  [mysqld]
  #缓冲池大小= (innodb_buffer_pool_instances *innodb_buffer_pool_chunk_size（默认128m）) * 倍数，innodb_buffer_pool_size必须是(innodb_buffer_pool_instances * innodb_buffer_pool_chunk_size（默认128m）)的倍数，否则会自动进行调整
  innodb_buffer_pool_size=2G
  # buffer pool 的数量，默认是1个，为了提高并发能力可以配置多个
  innodb_buffer_pool_instances=4
  #默认值 128m(134217728字节)
  innodb_buffer_pool_chunk_size=134217728
  ```

  

```markdown

=====================================
2021-01-20 02:50:51 0x7f33c8281700 INNODB MONITOR OUTPUT
=====================================
Per second averages calculated from the last 56 seconds
-----------------
BACKGROUND THREAD
-----------------
srv_master_thread loops: 5 srv_active, 0 srv_shutdown, 476 srv_idle
srv_master_thread log flush and writes: 481
----------
SEMAPHORES
----------
OS WAIT ARRAY INFO: reservation count 19
OS WAIT ARRAY INFO: signal count 16
RW-shared spins 0, rounds 7, OS waits 3
RW-excl spins 0, rounds 0, OS waits 0
RW-sx spins 0, rounds 0, OS waits 0
Spin rounds per wait: 7.00 RW-shared, 0.00 RW-excl, 0.00 RW-sx
------------
TRANSACTIONS
------------
Trx id counter 409961
Purge done for trx's n:o < 0 undo n:o < 0 state: running but idle
History list length 0
LIST OF TRANSACTIONS FOR EACH SESSION:
---TRANSACTION 421337907277456, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421337907276536, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
---TRANSACTION 421337907275616, not started
0 lock struct(s), heap size 1136, 0 row lock(s)
--------
FILE I/O
--------
I/O thread 0 state: waiting for completed aio requests (insert buffer thread)
I/O thread 1 state: waiting for completed aio requests (log thread)
I/O thread 2 state: waiting for completed aio requests (read thread)
I/O thread 3 state: waiting for completed aio requests (read thread)
I/O thread 4 state: waiting for completed aio requests (read thread)
I/O thread 5 state: waiting for completed aio requests (read thread)
I/O thread 6 state: waiting for completed aio requests (write thread)
I/O thread 7 state: waiting for completed aio requests (write thread)
I/O thread 8 state: waiting for completed aio requests (write thread)
I/O thread 9 state: waiting for completed aio requests (write thread)
Pending normal aio reads: [0, 0, 0, 0] , aio writes: [0, 0, 0, 0] ,
 ibuf aio reads:, log i/o's:, sync i/o's:
Pending flushes (fsync) log: 0; buffer pool: 0
2706 OS file reads, 62 OS file writes, 7 OS fsyncs
0.00 reads/s, 0 avg bytes/read, 0.05 writes/s, 0.00 fsyncs/s
-------------------------------------
INSERT BUFFER AND ADAPTIVE HASH INDEX
-------------------------------------
Ibuf: size 1, free list len 0, seg size 2, 0 merges
merged operations:
 insert 0, delete mark 0, delete 0
discarded operations:
 insert 0, delete mark 0, delete 0
Hash table size 553253, node heap has 2 buffer(s)
Hash table size 553253, node heap has 0 buffer(s)
Hash table size 553253, node heap has 1 buffer(s)
Hash table size 553253, node heap has 2 buffer(s)
Hash table size 553253, node heap has 0 buffer(s)
Hash table size 553253, node heap has 0 buffer(s)
Hash table size 553253, node heap has 0 buffer(s)
Hash table size 553253, node heap has 2 buffer(s)
2.16 hash searches/s, 0.98 non-hash searches/s
---
LOG
---
Log sequence number 3129215584
Log flushed up to   3129215584
Pages flushed up to 3129215584
Last checkpoint at  3129215575
0 pending log flushes, 0 pending chkp writes
10 log i/o's done, 0.00 log i/o's/second
----------------------
BUFFER POOL AND MEMORY
----------------------
Total large memory allocated 2198863872
Dictionary memory allocated 735270
Buffer pool size   131072
Free buffers       128455
Database pages     2610
Old database pages 856
Modified db pages  0
Pending reads      0
Pending writes: LRU 0, flush list 0, single page 0
Pages made young 0, not young 0
0.00 youngs/s, 0.00 non-youngs/s
Pages read 2575, created 35, written 45
0.00 reads/s, 0.00 creates/s, 0.00 writes/s
Buffer pool hit rate 1000 / 1000, young-making rate 0 / 1000 not 0 / 1000
Pages read ahead 0.00/s, evicted without access 0.00/s, Random read ahead 0.00/s
LRU len: 2610, unzip_LRU len: 0
I/O sum[0]:cur[0], unzip sum[0]:cur[0]
----------------------
INDIVIDUAL BUFFER POOL INFO
----------------------
---BUFFER POOL 0
Buffer pool size   32768
Free buffers       32099
Database pages     667
Old database pages 266
Modified db pages  0
Pending reads      0
Pending writes: LRU 0, flush list 0, single page 0
Pages made young 0, not young 0
0.00 youngs/s, 0.00 non-youngs/s
Pages read 667, created 0, written 2
0.00 reads/s, 0.00 creates/s, 0.00 writes/s
Buffer pool hit rate 1000 / 1000, young-making rate 0 / 1000 not 0 / 1000
Pages read ahead 0.00/s, evicted without access 0.00/s, Random read ahead 0.00/s
LRU len: 667, unzip_LRU len: 0
I/O sum[0]:cur[0], unzip sum[0]:cur[0]
---BUFFER POOL 1
Buffer pool size   32768
Free buffers       32314
Database pages     452
Old database pages 0
Modified db pages  0
Pending reads      0
Pending writes: LRU 0, flush list 0, single page 0
Pages made young 0, not young 0
0.00 youngs/s, 0.00 non-youngs/s
Pages read 452, created 0, written 0
0.00 reads/s, 0.00 creates/s, 0.00 writes/s
Buffer pool hit rate 1000 / 1000, young-making rate 0 / 1000 not 0 / 1000
Pages read ahead 0.00/s, evicted without access 0.00/s, Random read ahead 0.00/s
LRU len: 452, unzip_LRU len: 0
I/O sum[0]:cur[0], unzip sum[0]:cur[0]
---BUFFER POOL 2
Buffer pool size   32768
Free buffers       32188
Database pages     578
Old database pages 233
Modified db pages  0
Pending reads      0
Pending writes: LRU 0, flush list 0, single page 0
Pages made young 0, not young 0
0.00 youngs/s, 0.00 non-youngs/s
Pages read 578, created 0, written 0
0.00 reads/s, 0.00 creates/s, 0.00 writes/s
Buffer pool hit rate 1000 / 1000, young-making rate 0 / 1000 not 0 / 1000
Pages read ahead 0.00/s, evicted without access 0.00/s, Random read ahead 0.00/s
LRU len: 578, unzip_LRU len: 0
I/O sum[0]:cur[0], unzip sum[0]:cur[0]
---BUFFER POOL 3
Buffer pool size   32768
Free buffers       31854
Database pages     913
Old database pages 357
Modified db pages  0
Pending reads      0
Pending writes: LRU 0, flush list 0, single page 0
Pages made young 0, not young 0
0.00 youngs/s, 0.00 non-youngs/s
Pages read 878, created 35, written 43
0.00 reads/s, 0.00 creates/s, 0.00 writes/s
Buffer pool hit rate 1000 / 1000, young-making rate 0 / 1000 not 0 / 1000
Pages read ahead 0.00/s, evicted without access 0.00/s, Random read ahead 0.00/s
LRU len: 913, unzip_LRU len: 0
I/O sum[0]:cur[0], unzip sum[0]:cur[0]
--------------
ROW OPERATIONS
--------------
0 queries inside InnoDB, 0 queries in queue
0 read views open inside InnoDB
Process ID=1, Main thread ID=139860564391680, state: sleeping
Number of rows inserted 38, updated 0, deleted 0, read 81986
0.30 inserts/s, 0.00 updates/s, 0.00 deletes/s, 724.63 reads/s
----------------------------
END OF INNODB MONITOR OUTPUT
============================

```

- 重要的参数

  ```sh
  
  Total large memory allocated 2198863872 #buffer pool 的大小
  Buffer pool size   32768 #缓存页数量
  Free buffers       31854 #free链表中的空闲缓存页数量
  Database pages     913 # lru 链表中缓存页数量
  Old database pages 357 #lru链表中冷数据区缓存页数量
  Modified db pages  0 #flush链表中的缓存页数量
  Pending reads      0  # 等待从磁盘上加载进缓存页的数量
  
  # 即将从lru链表中刷入磁盘的数量、即将从flush链表中输入磁盘的数量
  Pending writes: LRU 0, flush list 0, single page 0 
  # 已经从冷数据区访问了转移到热数据区的的缓存页数量及速度，lru冷数据区1秒内被访问了没进入热数据区的缓存页数量及速度
  Pages made young 0, not young 0 0.00 youngs/s, 0.00 non-youngs/s
  # 已经读取、创建和写入了多少个缓存页，以及每秒的速度
  Pages read 878, created 35, written 43 0.00 reads/s, 0.00 creates/s, 0.00 writes/s
  # 每1000次访问有多少直接命中了缓存，每1000次访问有多少次访问让冷数据区的缓存页移动到了热数据区，没1000次访问没移动的缓存页数量
  Buffer pool hit rate 1000 / 1000, young-making rate 0 / 1000 not 0 / 1000
  # 
  Pages read ahead 0.00/s, evicted without access 0.00/s, Random read ahead 0.00/s
  # lru链表中缓存页的数量
  LRU len: 913, unzip_LRU len: 0
  # 最近50秒读取磁盘页的总数sum[0]，现在正在读取磁盘页的数量cur[0]
  I/O sum[0]:cur[0], unzip sum[0]:cur[0]
  ```



### 行溢出知道吗？

因为数据页的每页大小是16kb，并且数据都是放在数据页中的，如果一个字段的值设置的非常大比如varchar(65532) 、text、blob 这种类型的字段，这种就是超过了一个数据页16kb的大小，为了将数据放下，就会把数据分割到其他数据页中来存储，保证这个字段的数据放下，所以就会发生行溢出。

![](images/行溢出.png)


### 说说事务隔离级别有哪些？

事务产生的问题：

- 脏写
    当事务A将一行数据从null 更新为了a，然后事务B也来更新这行数据，更新成了b，然后事务A发现有问题，通过undo log 日志执行了事务回滚，这行数据从b变成了null，
    结果事务B一看自己更新的数据丢失了，出现了脏读问题
- 脏读
    当事务A执行了数据修改，把数据修改成了a，然后事务B查询到数据，然后去执行业务操作，但是事务A又回滚了，导致数据a变成了null，事务B再查询的时候发现
    数据丢失了
- 不可重复度
    前提条件，事务只能读到其他事务提交之后的数据，也就是不存在脏读的情况
    事务A 查询一个数据，拿到了数据为a，然后事务B和C都去修改这个值，并且修改值之后提交了事务，结果事务A再去查询的时候，发现数据已经不是a了，导致数据不可重复读
- 幻读
    事务A查询数据的时候，第一次查询10条，第二次查询到了事务B提交后的数据，返回了12条数据，查询到之前没查询过的数据

事务隔离级别：

- read uncommitted（读未提交）
    可以解决脏写问题，但是存在脏写、不可重复读、幻读问题
- read committed（读已提交）（RC）
    可以解决脏写、脏读问题，但是存在不可重复读、幻读问题
- repeatable read（可重复读）（RR）
    可以解决脏写、脏读、不可重复度问题，但是存在幻读问题
- serializable (串行化)
    单线程执行，可以解决所有问题，但是性能非常低下

### mysql 默认事务隔离级别是哪个？MVCC 多版本并发控制是如何解决幻读问题的？

![](images/mysql%20MVCC%20多版本并发控制链.png)

默认使用RR 级别，mysql 使用undo log 版本链解决了脏写、脏读、不可重复读、幻读问题

在RC 模式下，基于MCVV版本链，每次查询都会开启一个ReadView，ReadView就是在每次事务在进行快照读的时候生成的一个读视图，
在事务执行快照读的时候，会生成一个当前数据库的系统快照，并且维护了当前活跃的事务id（每开启一个事务都会生成一个事务id，并且是递增的）
在ReadView里面有m_ids 当前ReadView中的事务id列表， max_trx_id 最大的事务id（下一个要生成的事务id），min_trx_id 在m_ids中最小的事务id，
creator_trx_id 表示该事务生成ReadView时候的事务id，比如:m_ids=[34,35,37,50],max_trx_id=60,min_trx_id=34,creator_trx_id=37
表示生成这个ReadView的是事务37，在这个期间活动的事务id有34,35,37,50 下一个要生成的事务id是60

- 基于undo log 日志实现MVCC 
    在undo log 日志里面，会有两个隐藏的字段，一个是trx_id 表示事务id，一个是roll_pointer 表示指向下一个undo log 日志的指针
    当事务发起查询的时候：
    1）、如果当前版本链的undo log日志的事务id为37，而且creator_trx_id=37，表示这个这个是自己修改的值，是可以读取到的
    2）、如果当前版本链的undo log 日志的事务id小于min_trx_id，说明这个版本链在发起查询之前就已经对事务修改了，是可以读取到的
    3）、如果当前版本的事务id在min_trx_id和max_trx_id之间，如果在m_ids中，说明在同时开启事务的时候，其他事务也在修改这个数据，这个数据是不能读取的，需要通过roll_pointer
        指针往下找，找到比自己小的，或者等于自己的事务id的版本链，如果不在m_ids中，那么说明在创建这个ReadView的时候其他事务已经刚提交了，是可以访问的。
    4）、如果当前版本链的事务id大于等于max_trx_id ，说明在开启这个ReadView之后又开启了新的ReadView 并且修改了当前版本链，是不能访问到的

### MySQL 锁机制说下？

默认情况下MySQL查询是不加锁的，因为使用MVCC版本链快照读的方式，完全实现了事务之间的隔离，不用去关心数据脏读、脏写问题
默认情况下更新数据走的是独占锁，查询走的MVCC快照读
- 共享锁、独占锁
    加共享锁使用select * from table lock in share mode
    加独占锁使用select * from table for update
- ddl 语句默认是加了元数据锁的，会阻塞增删改操作

- 表锁 
    lock tables xxx read 表级共享锁
    lock tables xxx write 表级独占锁
    在执行更新操作的时候，默认会加行级的独占锁，并且默认会加表级的意向独占锁
    在执行查询的时候，默认会加表级的意向共享锁
    意向独占锁和意向共享锁不会互斥

### 刷脏页导致性能抖动的原因？如何调优？

抖动的原因：
- 执行了很多更新操作，产生了大量的脏页，当没有空闲缓存页的时候，需要将大量的脏页刷入磁盘，耗时
- 当redo log 写满了之后，默认是2个redo log，当写满之后就会刷入磁盘，因为写满之后他会重新从第一个日志文件写，会覆盖，所以必须刷入磁盘，耗时

调优：
- innodb_io_capacity=200 mysql5.7默认是200,表示io的随机读写性能-----> 使用fio工具测试磁盘读写性能，然后配置为随机读写的io大小
- innodb_flush_neighbors=1 默认是打开的，表示在flush缓存页的时候，会将相邻的缓存页也刷入磁盘 -----> 设置0，关闭，不然相邻的缓存页也刷入磁盘
- innodb_max_dirty_pages_pct=75% 默认75%，表示脏页的比例，在刷盘的时候


### 聚族索引知道吗？

聚族索引是由主键索引组成的B+树，最底层是数据页，也就是叶子节点存放的是数据页，其他索引页就是主键目录组成的，索引页中存放的是数据页中的最小主键值
然后在上一层索引页保存的下一层索引对应的索引页号和索引页对应的最小主键值，这样就组成了一棵B+树

### 除了主键索引以外建立的索引叫什么？

二级索引，也是一棵B+树，叶子节点是索引字段+主键，然后一层一层组成的B+树（二级索引），排序的话就按索引字段进行排序的

### 什么是回表？

回表就是使用二级索引查询数据，查询到了主键和索引字段，但是其他字段在二级索引中过没有，所以还要根据主键去聚族索引查询，找到叶子节点的数据页，然后将要查询的字段全部返回出来

### 插入数据的时候是如何维护聚族索引的？

插入数据的时候，首先是将数据插入到数据页中，然后就会维护一个主键目录，主键目录存放最小的主键值，如果此时要查询，直接根据二分查找在主键目录查找就可以了，
因为数据页中的主键是从小到大排序的，当数据页超过两个的时候就会维护一个索引页来当做B+树的根节点，索引页中存放的就是数据页号+最小主键值，
当数据不断的插入，数据页也不断增加，然后索引页会分裂成多个索引页，索引页中存放的就是其他索引页号+索引页中对应的最小主键值，最终就形成了一棵B+树
如果存在二级索引字段，那么同时会维护一棵二级索引的B+树


### 执行计划知道吗？
mysql对于一条sql语句针对大量的数据表、聚族索引和二级索引是如何检查查询，如何筛选，如何过滤，如何使用函数、如何分组、如何排序，
也就是说底层是如何找到这个数据的

### mysql 查询语句执行过程？

先是连接器，与客户端建立连接，然后是查询缓存，如果缓存中有查询过的申请了语句，就直接使用缓存中的，没有就使用分析器，将sql解析成mysql语法能够
认识的数据结构，然后通过优化器，找到一条代价最低的执行计划，然后调用执行器执行sql语句


