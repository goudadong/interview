### 说说 redis 常见数据类型及应用场景

- string 

分布式锁

- list

消息队列

- hash

购物车

- set

点赞
可能认识的人


- zset

排行榜


### redis 除了使用缓存外还可以用来做什么？

- 分布式锁
使用 set ex nx + lua 脚本实现
  - 演变：
     1、只是用set nx + delete , 如果业务代码执行失败，无法解锁，导致死锁 --> 加过期时间
     2、使用 set nx + set ex + delete，如果在加过期时间的时候突然程序闪断，导致无法解锁，造成死锁 ---> 使用set ex nx 原子加锁
     3、使用 set ex nx + delete，如果加锁成功之后，由于业务执行时间比较长，超过了设置的过期时间，然后其他线程发现没有锁，于是加锁成功，这个时候
        业务执行完毕，准备删除锁，结果把别人的锁给删除了，然后其他线程也可以自己加锁，导致锁无法唯一保证   --> 加上自己的标签，判断如果是自己的才删除
     4、使用 set ex nx + 随机值 + delete，如果业务执行完毕准备删除的时候，需要先获取一下锁，判断如果是自己的才删除，但是判断和删除不是原子的，
        就会造成如果查询到了锁，但是由于网络原因，锁过期了，其他线程也成功加上了锁，这个时候再去判断，发现值确实值自己的，然后执行删除，结果把别人的锁删除了（key是一样的）
        ---> 使用lua脚本将查询锁和删除锁设置为原子的
     5、使用 set ex nx + 随机值 + lua 脚本，如果业务执行时间比较长，比设置的过期时间长，程序还没执行完，锁过期了，其他线程还是会加锁成功 --> 设置续期
     
     6、 使用redisson 实现lock+unlock 加锁解锁，并且他会自带看门狗机制，检查过期时间如果只有1/3了就自动续期，默认情况下会自动加上30秒的过期时间
        并且会每10秒钟设置一次过期时间，如果手动指定了过期时间，不会启动看门狗机制，时间到了就会过期

### 如果不使用lua脚本如何实现解锁原子性？

使用redis的事务机制
watch key 开启监听
multi 开启事务
get key  加入队列
delete key 加入队列
exec 原子执行所有命令 如果期间有修改，会返回nil，否则返回ok
unwatch 关闭解锁

```sh
127.0.0.1:6379> WATCH k1
OK
127.0.0.1:6379> MULTI 
OK
127.0.0.1:6379> set k1 c23
QUEUED
127.0.0.1:6379> set k2 f2
QUEUED
127.0.0.1:6379> exec
1) OK
2) OK
127.0.0.1:6379> UNWATCH 
OK
```

- 消息队列

### redis zset 为什么使用跳表而不是用B+树？


### redis 如何修改内存？

- 修改配置文件redis.conf 
    `maxmomery bytes`
- 使用命令修改
    `config set maxmomery 104857600`

通过`info memory` 查看
```bash
127.0.0.1:6379> info memory
# Memory
used_memory:2111312
used_memory_human:2.01M
used_memory_rss:5509120
used_memory_rss_human:5.25M
used_memory_peak:2214512
used_memory_peak_human:2.11M
used_memory_peak_perc:95.34%
used_memory_overhead:2057682
used_memory_startup:791264
used_memory_dataset:53630
used_memory_dataset_perc:4.06%
allocator_allocated:2570456
allocator_active:2924544
allocator_resident:5361664
total_system_memory:1021509632
total_system_memory_human:974.19M
used_memory_lua:37888
used_memory_lua_human:37.00K
used_memory_scripts:0
used_memory_scripts_human:0B
number_of_cached_scripts:0
maxmemory:1
maxmemory_human:1B
maxmemory_policy:noeviction
allocator_frag_ratio:1.14
allocator_frag_bytes:354088
allocator_rss_ratio:1.83
allocator_rss_bytes:2437120
rss_overhead_ratio:1.03
rss_overhead_bytes:147456
mem_fragmentation_ratio:2.61
mem_fragmentation_bytes:3397864
mem_not_counted_for_evict:0
mem_replication_backlog:1048576
mem_clients_slaves:33844
mem_clients_normal:183998
mem_aof_buffer:0
mem_allocator:jemalloc-5.1.0
active_defrag_running:0
lazyfree_pending_objects:0

``` 
默认情况下，是没有设置的，也就是如果是64为机器，那么内存最大是机器的内存大小，如果是32位机器，那么最大内存是3G

一般情况下设置为机器的3/4就可以了

### redis  过期策略有哪些？ 内存淘汰策略有哪些？

- 过期策略
   - 定时删除
        cpu定时去删除，但是占用大量占用cpu，比如在同一时间去处理很多key，性能低下，时间换空间
   - 惰性删除
        当再次去访问的时候，如果发现key过期就删除，如果没过期就继续用，但是这样会导致大量的key不会删除，占用内存，空间换时间
   - 定期删除
        定时删除和惰性删除的折中，每隔一段时间才去删除key，并且删除的key是随机去抽采，如果key过期了才去删除，可以通过控制删除操作的时长和执行频率去减少对cpu的时间影响
        但是如果执行的频率太频繁，或者执行的时间太长，则删除变成了定时删除，也会占用太多的cpu，如果执行的频率少，执行时间太短，则删除变成了惰性删除，也会占用大量的内存
        所以必须得合理的设置执行时长和执行频率
        
        
- 内存淘汰策略
    - lru 最近最少使用淘汰策略
        - volatile-lru 表示最近最少使用的设置了过期时间的key
        - allkeys-lru 表示对所有的key使用最近最少过期淘汰策略
    - lfu 最近使用频率最少淘汰策略
        - volatile-lfu 表示对设置了过期时间的key使用最不常用使用频率最少的算法进行淘汰
        - allkeys-lfu 表示对所有的key使用频率最少的算法进行淘汰
    - random 随机策略
        - volatile-random 表示对设置了过期时间的可以使用随机淘汰策略
        - allkeys-random 表示对所有的key使用随机算法记性淘汰
    - volatile-ttl 表示对最近已经要过期的key进行淘汰
    - noeviction 不使用淘汰策略，内存不够了直接报错 
 
- 如何设置淘汰策略？
    修改配置文件 `maxmemory_policy=volatile-lru`
    使用命令修改 `config set maxmemory-policy volatile-lru`
    

### redis 持久化机制有哪些？

- rdb 快照机制
文件保存的完整不如aof，保存的是每个时刻的快照数据
优点：恢复速度快，保存某一时刻的完整数据，适合做冷备份
缺点：数据不完整

- aof 文件追加方式

每次写操作数据都会追加在aof文件尾部，数据是完整的，通过fsync模式保存数据，默认是每秒中保存这一次
fsync 三种方式：
1、appendfsync always 每次写操作都执行一次fsync保存，数据绝对不对丢失，但是性能急剧下降
2、appendfsync everysec 每秒中从os cache中fsync保存一次，性能高，最多丢失1秒的数据
3、appendfsync no 不使用fsync ，只是从os cache 中刷新磁盘中，完全取决于os cache，不安全

优点：数据完整，适合做数据备份
缺点：恢复速度慢

  - 如果文件不断变大，内存占满了怎么办？
   aof 会有一个rewrite机制，当达到一定的内存大小之后就会将数据重新写入一个新的aof文件中然后覆盖老的aof文件
   
   
   
### 说下redis集群方式有哪些?

- 主从架构模式

一主多从，不具备自动切换master，读写分离，可以抗10w以上的qps/s

- 哨兵+主从架构模式

在主从架构模式上增加哨兵集群模式，通过哨兵可以实现master和slave的自动主备切换
一般情况下，当一个哨兵发现master宕机了，那么就主观认为master宕机了叫sdown，如果有过半的数量的哨兵发现master宕机了，那么就客观宕机叫odown
并且当有qrnum的数量投票给一个哨兵，那么这个哨兵就可以执行主备切换了
master 和slave 是通过异步的方式同步数据的，数据也会出现丢失的情况，结局方案可以使用降级的方式
    - 如果发生脑裂了，怎么办？
    脑裂就是在发生网络分区故障的时候，导致master节点不能与其他slave通信，从而造成哨兵认为master宕机，然后执行主备切换，产生了两个master，当网络
    恢复之后，原来的master要连入集群中，就会导致部分数据丢失
    - 解决方案
    默认是打开了：
    ```sh
    min-replicas-to-write 1 
    min-replicas-max-lag 10
    ```
    当master同步数据给一个slave 超过了10秒钟的延迟，那么master就会停止接收客户端的写入请求，然后可以客户端采用降级的方式，比如写入到消息队列中，等待master
    恢复了再从消息队列中写入
    
- redis cluster 架构模式
使用hash slot 的方式将数据均匀分布在多个master节点上，默认是有16384个slot，通过取模的方式将数据均匀的分布在不停的master节点上，然后master节点再同步数据
给slave，经典的是三主三从的模式，即一个master对应一个slave节点


### 缓存穿透问题
问题：如果用户一直查询一个不存在的key，并且在同一时间高并发的请求，就会导致缓存穿透直接查询db，然后将db打死

解决方案： 将查询的null结果加入到缓存中，并且设置过期时间

### 缓存雪崩
问题：缓存不同数据的时候，设置了同一过期时间，导致某个时间内所有的key全部失效，然后将请求全部达到db上，导致db崩溃

解决方案：在设置过期时间的时候，加一个随机值，这样不同的key就不会在同一时间内过期而导致请求在同一时间内打到db上

### 缓存击穿
问题：在某个热点数据中，因为设置了过期时间，比如在某个时间key过期了，然后这个时候因为这个key是一个热点数据，比如是新版iPhone，
同时有100w的请求过来查询，因为key过期了，所以就会直接查询db，导致db崩溃，从而击穿了缓存

解决方案： 加锁，给发过来的请求加锁，如果第一个人查询到了数据，并且缓存起来，那么后面的所有请求都走缓存就不会导致缓存击穿

### 缓存一致性问题
有两种方式
1、双写模式
比如当更新了数据库，然后就去更新缓存  问题： 当并发写的情况下，第一个线程执行了数据库的更新，但是更新缓存的时候比较慢，然后第二个线程也执行了数据库更新，并且缓存也执行更新，
而且第一个线程还没开始执行完更新缓存，第二个已经执行完毕，这个时候第一个线程开始执行更新缓存，那么缓存中的数据就不是最新的数据 从而导致缓存双写不一致
解决方案：给缓存加上过期时间，并且在执行双写的时候加锁

2、失效模式
比如当更新了数据库，直接将缓存删掉  问题：在并发的情况下，如果有多个写操作和读操作，当线程1执行了更新并且删除了缓存，然后线程二开始更新数据库，但是因为网络延迟，数据库还没有更新，删除操作也没有执行，
这个时候第三个线程开始读取数据，但是第三个发现没有在缓存中读取到数据，这个时候他就去查询数据库，查到了线程1更新的数据，但是还没有进行缓存数据，这时线程二执行了数据库更新，并且也执行删除操作，然后第三个
线程执行缓存更新，更新的数据是第一个线程执行完的数据，就导致了数据不是最新的，造成缓存不一致
解决方案： 给缓存加上过期时间，并且加锁读写锁

3、两种模式的终极解决方案
使用canal binglog 日志进行数据一致性的解决
https://github.com/alibaba/canal
