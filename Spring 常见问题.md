### 说说 `Spring` `IOC` 是什么？

 - 依赖注入，控制反转
 
 - 使用`xml` + 工厂模式 + 反射实现的
 
 - 使用`BeanFactory` 和 `ApplicationContext` 来创建对象
    `BeanFactory` 创建对象是在使用的时候才会创建，一般是由`Spring` 来使用，`ApplicationContext` 是在加载的时候就创建了对象
    `ApplicationContext` 实现类有 `ClasspathXmlApplicationContext` 和 `FileSystemXmlApplicationContext` 两个，第一个是加载`classPath` 路径下的xml文件进行创建对象，
     第二个是使用的全路径
     
 - 注入方式： set 注入、构造注入、命名空间注入
 
 - `Spring` 中有两种类型的`Bean`：一种是普通类型的`bean`，一种是`FactoryBean`
    普通类型的`bean` 就是直接获取到的`bean` ，`FactoryBean` 是继承了`FactoryBean`接口的重写了`getObject`方法后的`Bean`
    
 - `Spring` 作用域 使用 `scope` 配置
    默认是单例 `singleton`、 多实例 `prototype` 、请求域`request` 、回话域`session`
    在设置为 `singleton` 的时候，默认是在配置`Spring`的时候就创建了对象，在设置为`prototype` 的时候，是在使用的时候才会去创建对象，是懒加载的，每次创建一个新的
 
 - IOC 操作Bean管理的生命周期 （7步）
    1、调用无参构造方法进行初始化
    2、调用setter 方法进行属性赋值
    3、调用后置处理器的前置方法 `postProcessorBeforeInitialization`（实现了`BeanPostProcessor`后置处理器方法）
    4、调用init-method 初始化方法 (手动指定)
    5、调用后置处理器的后置方法 `postProcessorAfterIniti alization` （实现了`BeanPostProcessor`后置处理器方法）
    6、使用bean调用对象，开始使用
    7、调用销毁方法（手动指定，容器关闭的时候执行）
 
 - `IOC` `Bean` 管理的方式
    - `xml`方式
        使用`<bean id="" class=""> `进行配置
    - 注解方式
        `@Autowired` 使用类型进行自动装配
        `@Qualifier` 使用名称进行分装配，需要使用`@Autowired`配合使用
        `@Resource` 可以根据类型和名称进行装配
        `@Value` 设置普通类型的变量
        `@Configuration` 设置该类为配置类相当于xml配置文件并使用`@ComponentScan` 指定扫描的包路径
 
### 请说一下 `Spring` `AOP` ？

面向切面编程，在不修改源代码的基础上进行方法的增加操作

- 有接口的情况下 使用`JDK` 动态代理进行实现
    创建接口实现类的代理对象，增强类的方法
    
- 没有接口的情况下 使用`CGLIB` 动态代理进行实现
    创建子类的代理对象，增强类的方法
 
- `AOP` 的术语
    - 连接点
        可以增强的方法
    - 切入点
        真正增强的的方法
    - 通知
        在被增强的方法的逻辑部分
        - 前置通知 `@Before`
        - 后置通知 `@AfterReturning`
        - 环绕通知 `@Around`
        - 异常通知 `@AfterThrowing`
        - 最终通知 `@After`
    - 切面
        把通知应用到切入点的过程（动作）
        
 - `Spring` 使用 `AspectJ` 实现`AOP`操作，一种是基于`xml` ，一种是基于注解
    切入点表达式： `execution`([权限修饰符][返回方法][全类路径][方法名称]([参数列表])) 
    举例：`com.redwinter.dao.UserDao#add`
    1、execution(* com.redwinter.dao.UserDao.add(..)) 表示对add方法进行增强
    2、execution(* com.redwinter.dao.UserDao.*(..)) 表示对UserDao 类下的所有方法进行增强
    3、execution(* com.redwinter.dao.*.*(..)) 表示对com.redwinter.dao 包下的所有类的所有方法进行增强
    
 - 如何使用
    1、在增强类中添加`@Aspect`，并使用`@Component`加入`Spring` 容器
    2、定义通知方法，并使用切入点表达式定义需要增强的方法
    3、使用`@PointCut` 注解抽取公共的切入点
    4、在增强类中使用@Order 配置优先级，表示对同一个方法增强的时候，`@Order` 的值越小优先级越大，越先执行
    5、使用`@EnableAspectJAutoProxy(proxyTargetClass = true)`开启`Aspect `
    ```java
    @Component
    @Aspect
    @Order(value = 1)
    @EnableAspectJAutoProxy(proxyTargetClass = true) // 打开aspect 扫描
    public class AspectProxy {
    
        @Pointcut("execution(* com.redwinter.study.service.*.*(..))")
        public void pointCut(){}
    
        @Before("pointCut()")
        public void before(){
            System.out.println("前置通知...before");
        }
    
        @AfterReturning("pointCut()")
        public void afterReturning(){
            System.out.println("后置通知...afterReturning");
        }
        @Around("pointCut()")
        public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
            System.out.println("环绕通知前...");
            Object proceed = joinPoint.proceed();
            System.out.println("环绕通知后...");
            return proceed;
        }
        @AfterThrowing("pointCut()")
        public void afterThrowing(){
            System.out.println("异常通知...");
        }
        @After("pointCut()")
        public void after(){
            System.out.println("最终通知...");
        }
    }
  ``
  
### `Spring` 事务管理 ？

要么一起成功，要么一起失败

- 事务的特性（ACID）
    - 原子性（atomic）
    - 一致性（consistency）
    - 隔离性（isolation）
    - 持久性（duration）

- Spring 事务管理（底层使用AOP进行实现）
    - 编程式事务
    - 声明式事务（一般用这个）
        - 基于xml
        - 基于注解

- Spring 事务传播行为
    - REQUIRED 表示如果有事务，当前方法就在事务中运行，如果没有就新建一个事务来运行
    - REQUIRED_NEW 表示每次都新建一个事务来运行，如果已经存在事务就把它挂起，并且重开一个事务运行
    - SUPPORTS 表示如果有事务，当前方法就在事务中运行，没有就直接非事务执行
    - NOT_SUPPORTS 表示当前方法不应该在事务中运行，如果有就把它挂起
    - MANDATORY 表示当前方法必须在事务中运行，没有就抛出异常
    - NEVER 表示当前方法不应该运行在事务中，如果有事务，就抛出异常
    - NESTED 表示如果有事务，当前方法就在嵌套事务中执行，没有就新建一个事务，在自己的事务中执行
    
- 事务隔离级别
    - READ_UNCOMMITTED 读未提交 存在脏读、不可重复度、幻读问题
    - READ_COMMITTED 读已提交 存在不可重复读、幻读问题
    - REPEATABLE_READ 可重复读 存在幻读问题
    - SERIALIZABLE 串行化 都不存在，单线程

### Spring 5 新特新？

- @Nullable 表示可以为空，用在方法上，方法参数上，属性上

### `Spring` 4 `和Spring` 5 中 `AOP` 方法调用执行是什么样的？

### `Spring` 是如何解决循环依赖的？

### `Spring` 的扩展点是如何实现的？


 
