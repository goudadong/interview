#### jvm 常见参数：
-Xmx512m 最大堆内存大小
-Xms512m 最小堆内存大小
-Xmn200m 年轻代内存大小 老年代即最大堆内存减去年轻代内存
-Xss1m 虚拟机栈内存大小
-XX:MetaspaceSize=1024m 永久代内存大小 java 8 以后
-XX:MaxMetaspaceSize=1024m 最大永久代内存大小 java 8 以后
-XX:PermSize=1024m 永久代内存大小 java 8以前
-XX:MaxPermSize=1024m 最大永久代内存大小 java 8以前
-XX:MaxTenuringTreshold=15 新生代最大存活年龄
-XX:PretenureSizeThreshold=1048576 字节数 1m 创建的对象如果大于这个值则直接进入老年代
-XX:SurvivorRatio=8 默认情况下，新生代占比Eden区域占8 from 和to 区域各占1 
-XX:DisableExplicitGC 禁用显示执行GC
-XX:+CMSParallelInitialMarkEnabled  开启cms 初始标记阶段执行多线程并发标记，多线程下虽然还是会STW，但是执行效率跟高，STW的时间就短一些 
-XX:+CMSScavengeBeforeRemark  开启cms 重新标记阶段 先尽量执行一次YGC 
-XX:+TraceClassLoading -XX:+TraceClassUnloading 开启 追踪class 创建或者卸载的情况
-XX:SoftRefLRUPolicyMSPerMB=1000 jvm 通过反射创建的弱引用的类的引用时间 默认是1000毫秒 这个参数如果设置太细，会导致反射创建的弱引用对象在短时间内失去引用，然后gc后给回收掉，这样在元空间就会不断的创建这写类，很快就会占满元空间，一旦元空间内存不足就会触发full gc
-XX:+UseCMSCompactAtFullCollection -XX:CMSFullGCsBeforeCompaction=0 开启压缩 减少内存碎片的产生，默认是5次 FULL GC  压缩一次


jstat -gc 7609  5000  gc 查看命令 7609 为java进程号 5000 为5秒中执行一次打印

#### 查看jvm 参数命令：
java -XX:+PrintFlagsFinal 查看修改更新  (= 没有修改过  := 人为修改过)
java -XX:+PrintFlagsInitial  --查看出厂默认值

#### XX 参数：
Boolean类型：公式：-XX:+ (+表示开启 -表示关闭) 例如：-XX:+PrintFlagsFinal
KV设值类型：公式：-XX:属性key=属性值value  例如：-XX:MetaspaceSize=21807104

#### jvm 垃圾回收器：
Serial  新生代垃圾回收器
Serial Old  老年代垃圾回收器
单线程工作原理

ParNew 新生代
CMS垃圾回收器 标记清除算法 + 标记整理算法 老年代
多线程并发机制

G1垃圾回收器 标记整理算法 新生代和老年代

#### 设置新生代ParNew垃圾回收器：
-XX:+UseParNewGC 设置新生代垃圾回收器为ParNew
-XX:ParallelGCThreads=5 设置ParNew 垃圾回收器线程数量 默认为0，会自动根据服务器cpu核数进行调整

#### 设置老年代垃圾回收器：
-XX:CMSInitiatingOccupancyFraction=92 表示老年代内存使用达到92%时触发垃圾回收
-XX:+UseCMSCompactAtFullCollection 默认打开 表示full GC 之后，再次进入STW，停止工作线程，进行碎片整理（标记整理算法）
-XX:CMSFullGCsBeforeCompaction=5 默认为5 表示执行多少次Full GC 之后进行一次整理

#### G1垃圾回收器中：
-XX:InitiatingHeapOccupancyPercent=45 默认值45 表示如果老年代中占用了45%的Region的时候就会触发Monior GC 和Full GC 混合回收.
-XX:G1HeapRegionSize 指定region 的大小 （1M~32M 必须是2的幂）
-XX:G1HeapWastePercent=5 表示空闲的Region数量达到堆内存的5%，就会立即停止混合回收
-XX:G1MixedGCCountTarget=8 表示G1 Mixed GC 混合回收在最后一个阶段执行多少次，默认8次，他会回收一次，系统停顿一次
-XX:G1MixedGCLiveThresholdPercent=85% 表示回收的Region 的时候，存活的对象必须小于85%才进行回收，如果大于85%，存活的对象多，回收将没有意义

#### GC名词解释：
Minor GC / Young GC ：新生代或者年轻代触发的GC
Old GC ：老年代发生的GC，有时可能会叫成Full GC
Full GC: 新生代、老年代和永久代整体进行的一次GC
Major GC : 很容混淆，建议不要提
Mixed GC : G1 垃圾回收器中的混合回收（新生代、老年代、大对象都会回收）发生的GC，一旦老年代占用堆内存的45%了就会触发Mixed GC ,此时新生代和老年代都会触发GC

#### JVM 启动时出现多次Full GC 原因：
方法区（元空间）内存太小导致，默认情况下为20.8m,可以调整为128m 即可
-XX:MetaspaceSize=128m

#### JVM通用模板 4核8G 服务器
-XX:MetaspaceSize=512m -XX:MaxMetaspaceSize=512m -XX:InitialHeapSize=4G -XX:MaxHeapSize=4G -XX:NewSize=3G -XX:MaxNewSize=3G -Xss1m
-XX:SurvivorRatio=8 -XX:PretenureSizeThreshold=10m -XX:MaxTenuringThreshold=15 -XX:+UseParNewGC -XX:+UseConcMarkSweepGC 
-XX:+UseCMSCompactAtFullCollection -XX:CMSFullGCsBeforeCompaction=1 -XX:+CMSParallelInitialMarkEnabled -XX:+CMSScavengeBeforeRemark
-XX:CMSInitiatingOccupancyFraction=92 -XX:+UseCMSInitiatingOccupancyOnly -XX:+CMSClassUnloadingEnabled 
-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/usr/local/app/oom.dump -XX:+DisableExplicitGC 
-XX:+PrintHeapAtGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:gc.log 

这个在排查时加入：-XX:+TraceClassLoading -XX:+TraceClassUnLoading -XX:SoftRefLRUPolicyMSPerMB=3000
不建议设置：-XX:+ParallelRefProcEnabled -XX:HeapDumpPath=jvm.dump


